#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
name: package_generator.py
author: Shlomi Ben-David (shlomi.ben.david@gmail.com)
description: The package_generator is tool that used to generate python packages.
"""
import os
import sys
import logging
from shutil import copytree
from pylib3 import init_logging
from string import Template
from glob import glob
from pygenerator3.args import get_cli_args

logger = logging.getLogger(__name__)
BASE_PATH = os.path.dirname(__file__)
TEMPLATES_DIR = os.path.join(BASE_PATH, 'templates')


def handle_package_dir(src_path, dst_path):
    """
    Handles package directory

    :param str src_path: source directory path
    :param str dst_path: destination directory path
    """
    dir_name = os.path.basename(src_path)
    if dir_name != 'package':
        return

    logger.debug(f"src_path: {src_path}")
    logger.debug(f"dst_path: {dst_path}")
    try:
        copytree(src=src_path, dst=dst_path)
        logger.info(f"Copying '{src_path}' directory to '{dst_path}'")
    except FileExistsError:
        pass


def handle_template_file(src_path, dst_path, **items):
    """
    Handle template file

    :param src_path: source file path
    :param dst_path: destination file path
    """
    logger.debug(">" * 80)
    file_name = os.path.basename(src_path).replace('.template', '')
    dst_file_path = os.path.join(dst_path, file_name)

    if file_name in ["__version__.py"]:
        dst_file_path = os.path.join(dst_path, items['package_name'], file_name)
    logger.debug(f"Reading from '{src_path}' file")
    with open(src_path) as ifile:
        data = ifile.read()

    s = Template(data)
    new_data = s.safe_substitute(**items)
    logger.info(f"Updating '{file_name}' file content")

    try:
        logger.debug(f"Writing to '{dst_file_path}' file")
        with open(dst_file_path, 'w') as ofile:
            ofile.write(new_data)
    except PermissionError as err:
        logger.error(err)

    logger.debug("<" * 80)


def main():
    """ MAIN """

    # get commandline arguments
    args = get_cli_args()

    # init logger
    init_logging(
        log_file=args.log_file,
        console=args.console,
        verbose=args.verbose
    )

    package_name = args.package_name
    version_file = \
        args.version_file or "__version__.py"
    dst_path = args.dst or os.path.realpath('.')

    logger.info("Generating package structure")

    # create package primary directory
    primary_dir_path = os.path.join(dst_path, package_name)
    if not os.path.exists(primary_dir_path):
        logger.info(f"Creating '{primary_dir_path}' directory")
        os.makedirs(primary_dir_path)

    logger.info("Collecting template files")
    template_files = glob(TEMPLATES_DIR + '/.*') + glob(TEMPLATES_DIR + '/*')
    logger.debug(f"template files: {template_files}")

    items = {
        'package_name': package_name,
        'author': args.author,
        'author_email': args.author_email,
        'description': args.description,
        'url': args.url,
        'version_file': version_file,
        'python_version': args.python_version,
        'package_version': args.package_version
    }

    src_package_path = os.path.join(TEMPLATES_DIR, "package")
    dst_package_path = os.path.join(primary_dir_path, package_name)
    handle_package_dir(src_path=src_package_path, dst_path=dst_package_path)

    for temp_path in template_files:
        if os.path.isdir(temp_path):
            continue

        handle_template_file(src_path=temp_path, dst_path=primary_dir_path, **items)
    logger.info(f"Package '{package_name}' successfully created")


if __name__ == '__main__':
    sys.exit(main())
